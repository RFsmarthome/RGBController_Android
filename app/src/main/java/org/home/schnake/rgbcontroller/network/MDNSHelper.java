package org.home.schnake.rgbcontroller.network;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.util.Log;

import org.home.schnake.rgbcontroller.BuildConfig;
import org.home.schnake.rgbcontroller.daten.NodeAdapterInterface;
import org.home.schnake.rgbcontroller.daten.NodeInfo;

public class MDNSHelper {
	private static final String SERVICE_TYPE = "_sbcoap._udp.";
	private static final String TAG = "Discover";

	protected NsdManager m_nsdManager;
	protected NsdManager.DiscoveryListener m_discoveryListener;
	protected Context m_context;

	public MDNSHelper(Context context) {
		m_context = context;
	}

	public void initDiscoveryListener(final NodeAdapterInterface adapter) {
		Log.d(TAG, "initDiscoveryListener");
		m_nsdManager = (NsdManager)m_context.getApplicationContext().getSystemService(Context.NSD_SERVICE);

		m_discoveryListener = new NsdManager.DiscoveryListener() {
			@Override
			public void onDiscoveryStarted(String regType) {
				Log.d(TAG, "Service discovery started");
			}

			@Override
			public void onServiceFound(final NsdServiceInfo service) {
				Log.i(TAG, "Service discovered: "+ service);

				m_nsdManager.resolveService(service, new NsdManager.ResolveListener() {
					@Override
					public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
						Log.e(TAG, "Error on discover: "+ errorCode);
					}

					@Override
					public void onServiceResolved(NsdServiceInfo serviceInfo) {
						Log.d(TAG, "ServiceInfo: "+ serviceInfo);
						adapter.addNode(serviceInfo);
					}
				});
			}
			@Override
			public void onServiceLost(NsdServiceInfo service) {
				Log.e(TAG, "service lost" + service);
				adapter.removeNode(service);
			}

			@Override
			public void onDiscoveryStopped(String serviceType) {
				Log.i(TAG, "Discovery stopped: " + serviceType);
			}

			@Override
			public void onStartDiscoveryFailed(String serviceType, int errorCode) {
				Log.e(TAG, "Discovery failed: Error code:" + errorCode);
			}

			@Override
			public void onStopDiscoveryFailed(String serviceType, int errorCode) {
				Log.e(TAG, "Discovery failed: Error code:" + errorCode);
			}
		};

		m_nsdManager.discoverServices(SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, m_discoveryListener);

		if(BuildConfig.DEBUG) {
			Log.d(TAG, "Added as debug infos");
			NodeInfo nodeInfo = NodeInfo.newInstance("DEBUG", "DEBUG", "DEBUG", "192.168.178.31", 5683, "DEBUG");
			adapter.addNode(nodeInfo);
		}
	}
}
