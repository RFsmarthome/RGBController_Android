package org.home.schnake.rgbcontroller;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.core.network.config.NetworkConfigDefaults;
import org.home.schnake.rgbcontroller.daten.NodeInfo;
import org.home.schnake.rgbcontroller.daten.NodesAdapter;
import org.home.schnake.rgbcontroller.daten.Preferences;
import org.home.schnake.rgbcontroller.network.MDNSHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class MainActivity extends AppCompatActivity {
	private static final String TAG = "MainActivity";
	private NodesAdapter m_nodesAdapter;
	protected static Context context;

	@BindView(R.id.toolbar) Toolbar m_toolbar;
	@BindView(R.id.listview_nodes) ListView m_listViewNodes;

	protected void initCoapFramework() {
		NetworkConfig nc = NetworkConfig.createStandardWithoutFile();
		NetworkConfigDefaults.setDefaults(nc);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		context = getApplicationContext();

		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);

		setSupportActionBar(m_toolbar);

		m_nodesAdapter = new NodesAdapter(m_listViewNodes);
		m_listViewNodes.setAdapter(m_nodesAdapter);

		MDNSHelper mdnsHelper = new MDNSHelper(this);
		mdnsHelper.initDiscoveryListener(m_nodesAdapter);

		initCoapFramework();
		Preferences.init(getApplicationContext());
	}

	@OnItemClick(R.id.listview_nodes)
	public void nodeClicked(AdapterView<?> parent, View view, int position, long id) {
		NodeInfo nodeInfo = (NodeInfo)m_nodesAdapter.getItem(position);
		Log.d(TAG, "Select listview item: "+ nodeInfo);

		Intent i = new Intent(this, DeviceActivity.class);
		i.putExtra(DeviceActivity.SERVICE_INFO_TAG, nodeInfo);
		startActivity(i);
	}

	public static Context getAppContext() {
		return context;
	}
}
