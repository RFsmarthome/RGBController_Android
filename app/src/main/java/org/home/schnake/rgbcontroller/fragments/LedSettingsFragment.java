package org.home.schnake.rgbcontroller.fragments;

import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.home.schnake.rgbcontroller.R;
import org.home.schnake.rgbcontroller.ScrollableSwipeRefreshLayout;
import org.home.schnake.rgbcontroller.databinding.FragmentLedSettingsBinding;
import org.home.schnake.rgbcontroller.daten.NodeInfo;
import org.home.schnake.rgbcontroller.daten.Settings;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class LedSettingsFragment extends Fragment {
	private static final String TAG = "LedSettingsFragment";

	private static final String ARG_COAP = "ARG_COAP";

	protected NodeInfo nodeInfo = null;

	protected Unbinder unbinder = null;

	@BindView(R.id.led_settings_frag_swipe)
	ScrollableSwipeRefreshLayout scrollableSwipeRefreshLayout;

	Settings settings;

	public LedSettingsFragment() {
	}

	public static LedSettingsFragment newInstance(NodeInfo nodeInfo) {
		LedSettingsFragment fragment = new LedSettingsFragment();
		Bundle args = new Bundle();
		args.putParcelable(ARG_COAP, nodeInfo);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		FragmentLedSettingsBinding fragmentLedSettingsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_led_settings, container, false);
		View view = fragmentLedSettingsBinding.getRoot();
		unbinder = ButterKnife.bind(this, view);

		Bundle bundle = getArguments();
		if(bundle != null) {
			nodeInfo = bundle.getParcelable(ARG_COAP);
		}

		settings = new Settings(nodeInfo);
		updateData();

		fragmentLedSettingsBinding.setSettings(settings);

		initRefreshListener();

		return view;
	}

	private void updateData() {
		new AsyncTask<Settings, Void, Boolean>() {
			@Override
			protected Boolean doInBackground(Settings... params) {
				Settings settings = params[0];
				return settings.loadData();
			}
		}.execute(settings);
	}

	protected void initRefreshListener() {
		scrollableSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				updateData();
			}
		});
	}

	public void onClickApply(View view) {
		saveSettings();
	}


	protected void saveSettings() {
		new AsyncTask<Settings, Void, Boolean>() {
			@Override
			protected Boolean doInBackground(Settings... params) {
				return null;
			}

			@Override
			protected void onPostExecute(Boolean aBoolean) {
				super.onPostExecute(aBoolean);
			}
		}.execute(settings);
	}

	@Override
	public void onDestroyView() {
		scrollableSwipeRefreshLayout.setRefreshing(false);
		super.onDestroyView();
		if(unbinder != null) {
			unbinder.unbind();
			unbinder = null;
		}
		nodeInfo = null;
		settings = null;
	}
}
