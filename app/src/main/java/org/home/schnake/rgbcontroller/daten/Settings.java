package org.home.schnake.rgbcontroller.daten;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableBoolean;
import android.util.Log;
import android.view.View;

import com.android.databinding.library.baseAdapters.BR;
import com.google.gson.Gson;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.MediaTypeRegistry;

public class Settings extends BaseObservable {
	static final private String TAG = "Settings";

	private String ssid;
	private String dev_ssid;
	private String password;
	private String gamma;
	private Float dev_gamma;
	private String desc;
	private String dev_desc;

	public ObservableBoolean isDataTransfer = new ObservableBoolean(false);

	protected CoapClient coapClient = null;
	protected NodeInfo nodeInfo = null;

	public static class SettingsCoap {
		public Float gamma = null;
		public String ssid = null;
		public String password = null;
		public String desc = null;
	}

	public Settings(NodeInfo nodeInfo) {
		super();
		this.nodeInfo = nodeInfo;

		coapClient = new CoapClient("coap", nodeInfo.getHostAddress(), nodeInfo.getPort(), "/settings");
	}

	@Bindable
	public String getSsid() {
		return ssid;
	}
	public void setSsid(String ssid) {
		this.ssid = ssid;
		notifyPropertyChanged(BR.ssid);
	}

	@Bindable
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
		notifyPropertyChanged(BR.password);
	}

	@Bindable
	public String getGamma() {
		return gamma;
	}
	public void setGamma(String gamma) {
		this.gamma = gamma;
		notifyPropertyChanged(BR.gamma);
	}

	@Bindable
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
		notifyPropertyChanged(BR.desc);
	}

	/*
	@BindingAdapter("android:text")
	public static void setFloat(TextView view, float value) {
		if(Float.isNaN(value)) {
			view.setText("");
		} else {
			view.setText(Float.toString(value));
		}
	}

	@InverseBindingAdapter(attribute = "android:text")
	public static float getFloat(TextView view) {
		String num = view.getText().toString();
		Log.d(TAG, "num:"+ num);
		if(num.isEmpty()) return 0.0F;
		try {
			return Float.parseFloat(num);
		}
		catch(NumberFormatException nfe) {
			return 0.0F;
		}
	}
	*/

	public void onLoadData(View view) {
		loadData();
	}

	public boolean loadData() {
		isDataTransfer.set(true);
		CoapResponse coapResponse = coapClient.get();
		if(coapResponse!=null && coapResponse.isSuccess()) {
			SettingsCoap settingsCoap = new Gson().fromJson(coapResponse.getResponseText(), SettingsCoap.class);
			if(settingsCoap != null) {
				dev_ssid = ssid = settingsCoap.ssid;
				password = null;
				dev_desc = desc = settingsCoap.desc;
				dev_gamma = settingsCoap.gamma;
				gamma = dev_gamma.toString();

				notifyChange();
				isDataTransfer.set(false);
				return true;
			}
		}

		isDataTransfer.set(false);
		return true;
	}

	public void onStoreData(View view) {
		storeData();
	}

	public boolean storeData() {
		isDataTransfer.set(true);
		SettingsCoap settingsCoap = new SettingsCoap();
		if(ssid != null && !ssid.equals(dev_ssid)) {
			settingsCoap.ssid = ssid;
		}
		if(desc != null && !desc.equals(dev_desc)) {
			settingsCoap.desc = desc;
		}
		if(gamma != null && !gamma.equals(dev_gamma.toString())) {
			try {
				Float num = Float.parseFloat(gamma);
				settingsCoap.gamma = num;
			}
			catch(NumberFormatException nfe) {}
		}
		if(password != null) {
			settingsCoap.password = password;
		}

		String json = new Gson().toJson(settingsCoap);
		Log.d(TAG, "JSON: "+ json);
		CoapResponse coapResponse = coapClient.put(json.getBytes(), MediaTypeRegistry.APPLICATION_JSON);
		if(coapResponse != null && coapResponse.isSuccess()) {
			dev_ssid = ssid;
			dev_desc = desc;
			dev_gamma = Float.parseFloat(gamma);

			isDataTransfer.set(false);
			return true;
		}

		isDataTransfer.set(false);
		return false;
	}
}
