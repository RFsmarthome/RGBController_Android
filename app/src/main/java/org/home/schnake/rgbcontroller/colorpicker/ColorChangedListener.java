package org.home.schnake.rgbcontroller.colorpicker;

public interface ColorChangedListener {
	void onColorChanged(int color, int oldColor);
}
