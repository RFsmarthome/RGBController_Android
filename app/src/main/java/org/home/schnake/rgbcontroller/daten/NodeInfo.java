package org.home.schnake.rgbcontroller.daten;

import android.net.nsd.NsdServiceInfo;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.Log;


import java.io.UnsupportedEncodingException;

public class NodeInfo implements Comparable, Parcelable {

	private static final String TAG = "NodeInfo";

	String id;
	String hostName;
	String hostAddress;
	String serviceName;
	String description;
	int port;
	NsdServiceInfo nsdServiceInfo;

	protected NodeInfo() {
		super();
	}

	public static NodeInfo newInstance(NsdServiceInfo serviceInfo) {
		NodeInfo nodeInfo = new NodeInfo();

		nodeInfo.nsdServiceInfo = serviceInfo;
		nodeInfo.id = serviceInfo.getServiceName();
		nodeInfo.port = serviceInfo.getPort();
		Log.d(TAG, "NodeInfo ID: "+ nodeInfo.id);
		if(serviceInfo.getHost()==null) {
			nodeInfo.hostName = "";
			nodeInfo.hostAddress = "";
		} else {
			nodeInfo.hostName = serviceInfo.getHost().getHostName();
			nodeInfo.hostAddress = serviceInfo.getHost().getHostAddress();
		}
		nodeInfo.serviceName = serviceInfo.getServiceName() == null ? "" : serviceInfo.getServiceName();
		try {
			byte[] desc = serviceInfo.getAttributes().get("desc");
			nodeInfo.description = desc == null ? "" : new String(serviceInfo.getAttributes().get("desc"), "ISO-8859-1");
		}
		catch(UnsupportedEncodingException nee) {
			nodeInfo.description = "";
		}

		return nodeInfo;
	}

	public static NodeInfo newInstance(String id, String serviceName, String hostName, String hostAddress, int port, String description) {
		NodeInfo nodeInfo = new NodeInfo();

		nodeInfo.id = id;
		nodeInfo.serviceName = serviceName;
		nodeInfo.hostName = hostName;
		nodeInfo.hostAddress = hostAddress;
		nodeInfo.description = description;
		nodeInfo.port = port;

		return nodeInfo;
	}

		public String getId() {
		return id;
	}
	public void setId(String m_id) {
		this.id = m_id;
	}

	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String m_serviceName) {
		this.serviceName = m_serviceName;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String m_description) {
		this.description = m_description;
	}

	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) { this.hostAddress = hostName; }

	public String getHostAddress() {return hostAddress; }
	public void setHostAddress(String hostAddress) { this.hostAddress = hostAddress; }

	public int getPort() { return port; }
	public void setPort(int port) { this.port = port; }

	@Override
	public int compareTo(@NonNull Object o) {
		if(o instanceof NodeInfo) {
			return ((NodeInfo)o).getId().compareTo(getId());
		}
		return 0;
	}

	@Override
	public boolean equals(@NonNull Object o) {
		if(o instanceof NodeInfo) {
			return ((NodeInfo)o).getId().equals(getId());
		}
		return false;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(android.os.Parcel dest, int flags) {
		dest.writeString(this.id);
		dest.writeString(this.hostName);
		dest.writeString(this.hostAddress);
		dest.writeString(this.serviceName);
		dest.writeString(this.description);
		dest.writeInt(this.port);
		dest.writeParcelable(this.nsdServiceInfo, flags);
	}

	protected NodeInfo(android.os.Parcel in) {
		this.id = in.readString();
		this.hostName = in.readString();
		this.hostAddress = in.readString();
		this.serviceName = in.readString();
		this.description = in.readString();
		this.port = in.readInt();
		this.nsdServiceInfo = in.readParcelable(NsdServiceInfo.class.getClassLoader());
	}

	public static final Parcelable.Creator<NodeInfo> CREATOR = new Parcelable.Creator<NodeInfo>() {
		@Override
		public NodeInfo createFromParcel(android.os.Parcel source) {
			return new NodeInfo(source);
		}

		@Override
		public NodeInfo[] newArray(int size) {
			return new NodeInfo[size];
		}
	};
}
