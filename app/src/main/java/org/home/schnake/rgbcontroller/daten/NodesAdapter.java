package org.home.schnake.rgbcontroller.daten;


import android.net.nsd.NsdServiceInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.home.schnake.rgbcontroller.R;

import java.util.LinkedHashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NodesAdapter extends BaseAdapter implements NodeAdapterInterface {
	private static final String TAG = "NodesAdapter";

	LinkedHashMap<String, NodeInfo> m_mapNodeInfo = new LinkedHashMap<>();
	View m_view;
	LayoutInflater m_layoutInflater;

	public NodesAdapter(View view) {
		m_view = view;
		m_layoutInflater = LayoutInflater.from(view.getContext());
	}

	public void addNode(NodeInfo nodeInfo) {
		m_mapNodeInfo.put(nodeInfo.getId(), nodeInfo);
	}

	public void addNode(NsdServiceInfo serviceInfo) {
		NodeInfo nodeInfo = NodeInfo.newInstance(serviceInfo);
		m_mapNodeInfo.put(nodeInfo.getId(), nodeInfo);
		m_view.post(new Runnable() {
			@Override
			public void run() {
				notifyDataSetChanged();
			}
		});
	}

	public void removeNodeById(String id) {
		m_mapNodeInfo.remove(id);
		m_view.post(new Runnable() {
			@Override
			public void run() {
				notifyDataSetChanged();
			}
		});
	}

	public void removeNode(NodeInfo nodeInfo) {
		removeNodeById(nodeInfo.getId());
	}

	public void removeNode(NsdServiceInfo nsdServiceInfo) { removeNode(NodeInfo.newInstance(nsdServiceInfo)); }

	@Override
	public int getCount() {
		return m_mapNodeInfo.size();
	}

	@Override
	public Object getItem(int position) {
		String[] keys = m_mapNodeInfo.keySet().toArray(new String[m_mapNodeInfo.size()]);
		return m_mapNodeInfo.get(keys[position]);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		Log.d(TAG, "convertView: " + view);
		ViewHolder holder;
		if(view != null) {
			holder = (ViewHolder)view.getTag();
		} else {
			view = m_layoutInflater.inflate(R.layout.item_node, parent, false);
			holder = new ViewHolder(view);
			view.setTag(holder);
		}

		String[] keys = m_mapNodeInfo.keySet().toArray(new String[m_mapNodeInfo.size()]);
		String key = keys[position];
		NodeInfo nodeInfo = m_mapNodeInfo.get(key);

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(nodeInfo.getServiceName());

		//String description = nodeInfo.getDescription();
		Log.d(TAG, "Description: "+ nodeInfo.getDescription());

		holder.nodeName.setText(nodeInfo.getDescription());

		stringBuilder.append(" (");
		stringBuilder.append(nodeInfo.getHostAddress());
		stringBuilder.append(")");

		holder.nodeIpAddress.setText(stringBuilder.toString());

		return view;
	}

	static class ViewHolder {
		@BindView(R.id.textview_node_name) TextView nodeName;
		@BindView(R.id.textview_node_ipaddress) TextView nodeIpAddress;

		public ViewHolder(View view) {
			ButterKnife.bind(this, view);
		}
	}
}
