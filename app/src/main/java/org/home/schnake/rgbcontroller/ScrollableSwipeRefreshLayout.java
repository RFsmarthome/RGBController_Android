package org.home.schnake.rgbcontroller;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;

/**
 * Created by schnake on 20.06.17.
 */

public class ScrollableSwipeRefreshLayout extends SwipeRefreshLayout {
	public ScrollableSwipeRefreshLayout(Context context) {
		super(context);
	}

	public ScrollableSwipeRefreshLayout(Context context, AttributeSet attr) {
		super(context, attr);
	}

	@Override
	public boolean canChildScrollUp() {
		return super.canChildScrollUp();
	}
}
