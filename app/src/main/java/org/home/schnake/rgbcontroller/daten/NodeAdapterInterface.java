package org.home.schnake.rgbcontroller.daten;

import android.net.nsd.NsdServiceInfo;

public interface NodeAdapterInterface {
	void addNode(NodeInfo nodeInfo);
	void addNode(NsdServiceInfo serviceInfo);
	void removeNodeById(String id);
	void removeNode(NodeInfo nodeInfo);
	void removeNode(NsdServiceInfo nsdServiceInfo);
}
