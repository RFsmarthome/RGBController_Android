package org.home.schnake.rgbcontroller;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;

import org.home.schnake.rgbcontroller.daten.NodeInfo;
import org.home.schnake.rgbcontroller.fragments.ColorFragment;
import org.home.schnake.rgbcontroller.fragments.LedSettingsFragment;

/**
 * Created by schnake on 01.07.17.
 */

public class DrawerManager {
	public static Fragment selectFragment(int index, NodeInfo nodeInfo, MenuItem item) {
		Fragment fragment = null;
		switch(item.getItemId()) {
			case R.id.nav_led_color:
				fragment = ColorFragment.newInstance(index, nodeInfo);
				break;
			case R.id.nav_led_program:
				break;
			case R.id.nav_led_settings:
				fragment = LedSettingsFragment.newInstance(nodeInfo);
				break;
			case R.id.nav_led_preferences:
				break;
			default:
				fragment = ColorFragment.newInstance(index, nodeInfo);
		}
		return fragment;
	}
}
