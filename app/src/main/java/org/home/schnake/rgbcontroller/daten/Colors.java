package org.home.schnake.rgbcontroller.daten;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableBoolean;
import android.util.Log;
import android.view.View;

import com.android.databinding.library.baseAdapters.BR;
import com.google.gson.Gson;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.home.schnake.rgbcontroller.MainActivity;

public class Colors extends BaseObservable {
	static final private String TAG = "Colors";
	static final private int DEFAULT_TIMEOUT = 100;

	private ColorsCoap current = new ColorsCoap();
	private ColorsCoap dev = new ColorsCoap();

	public ObservableBoolean isDataTransfer = new ObservableBoolean(false);

	protected CoapClient coapClient = null;
	protected NodeInfo nodeInfo = null;

	public static class ColorsCoap {
		public ColorsCoap() { super(); }
		public ColorsCoap(ColorsCoap o) {
			super();
			this.i = o.i;
			this.r = o.r;
			this.g = o.g;
			this.b = o.b;
			this.w = o.w;
			this.brightness = o.brightness;
		}
		public Integer i = null;
		public Integer r = null;
		public Integer g = null;
		public Integer b = null;
		public Integer w = null;
		public Integer brightness = null;
	}

	public Colors(NodeInfo nodeInfo) {
		super();
		this.nodeInfo = nodeInfo;

		coapClient = new CoapClient("coap", nodeInfo.getHostAddress(), nodeInfo.getPort(), "/rgbw");
		coapClient.setTimeout(Preferences.getTimeout());
	}

	public Colors(NodeInfo nodeInfo, int timeout) {
		super();
		this.nodeInfo = nodeInfo;

		coapClient = new CoapClient("coap", nodeInfo.getHostAddress(), nodeInfo.getPort(), "/rgbw");
		coapClient.setTimeout(timeout);
	}

	@Bindable
	public int getIndex() {
		return current.i == null ? 0 : current.i;
	}
	public void setIndex(int index) {
		this.current.i = index;
		notifyPropertyChanged(BR.index);
	}

	@Bindable
	public int getR() {
		return current.r == null ? 0 : current.r;
	}
	public void setR(int r) {
		this.current.r = r;
		notifyPropertyChanged(BR.r);
		notifyPropertyChanged(BR.rgb);
	}

	@Bindable
	public int getG() {
		return current.g == null ? 0 : current.g;
	}
	public void setG(int g) {
		this.current.g = g;
		notifyPropertyChanged(BR.g);
		notifyPropertyChanged(BR.rgb);
	}

	@Bindable
	public int getB() {
		return current.b == null ? 0 : current.b;
	}
	public void setB(int b) {
		this.current.b = b;
		notifyPropertyChanged(BR.b);
		notifyPropertyChanged(BR.rgb);
	}

	@Bindable
	public int getWhite() {
		return current.w == null ? 0 : current.w;
	}
	public void setWhite(int white) {
		this.current.w = white;
		notifyPropertyChanged(BR.white);
	}

	@Bindable
	public int getBrightness() {
		return current.brightness == null ? 0 : current.brightness;
	}
	public void setBrightness(int brightness) {
		this.current.brightness = brightness;
		notifyPropertyChanged(BR.brightness);
	}

	@Bindable
	public int getRgb() {
		return (0xff & current.r) << 16 | (0xff & current.g) << 8 | (0xff & current.b);
	}
	public void setRgb(int rgb) {
		current.r = (rgb & 0xff0000) >> 16;
		current.g = (rgb & 0xff00) >> 8;
		current.b = rgb & 0xff;

		notifyPropertyChanged(BR.r);
		notifyPropertyChanged(BR.g);
		notifyPropertyChanged(BR.b);
	}

	public void setRgb(int r, int g, int b) {
		current.r = r;
		current.g = g;
		current.b = b;

		notifyPropertyChanged(BR.r);
		notifyPropertyChanged(BR.g);
		notifyPropertyChanged(BR.b);
	}

	public void onLoadData(View view) {
		loadData();
	}

	public boolean loadData() {
		isDataTransfer.set(true);

		Log.d(TAG, "Load Colors data from device");

		CoapResponse coapResponse = coapClient.get();
		if(coapResponse != null && coapResponse.isSuccess()) {
			ColorsCoap[] colorsCoaps = new Gson().fromJson(coapResponse.getResponseText(), ColorsCoap[].class);
			int i = current.i;
			if(colorsCoaps != null && colorsCoaps.length > i) {
				current = new ColorsCoap(colorsCoaps[i]);
				dev = new ColorsCoap(current);

				notifyChange();
				isDataTransfer.set(false);
				return true;
			}
		}

		isDataTransfer.set(false);
		return false;
	}

	public void onStoreData(View view) {
		storeData();
	}

	public boolean storeData() {
		boolean send = false;
		ColorsCoap colorsCoap = new ColorsCoap();
		ColorsCoap tmp = new ColorsCoap(current);

		colorsCoap.i = tmp.i;

		if(tmp.r != dev.r || tmp.g != dev.g || tmp.b != dev.b) {
			send = true;
			colorsCoap.r = tmp.r;
			colorsCoap.g = tmp.g;
			colorsCoap.b = tmp.b;
		}

		if(tmp.w != dev.w) {
			send = true;
			colorsCoap.w = tmp.w;
		}

		if(tmp.brightness != dev.brightness) {
			send = true;
			colorsCoap.brightness = tmp.brightness;
		}

		String json = new Gson().toJson(colorsCoap);
		Log.v(TAG, "json: " + json);
		CoapResponse coapResponse = coapClient.put(json.getBytes(), MediaTypeRegistry.APPLICATION_JSON);
		if(coapResponse != null && coapResponse.isSuccess() && send) {
			dev = new ColorsCoap(tmp);
			return true;
		}
		return false;
	}
}
