package org.home.schnake.rgbcontroller.daten;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by schnake on 01.07.17.
 */

public class Preferences {
	private static final String PREF_NAME = "rgb_controller_prefs";

	private static final String TIMEOUT = "timeout";

	private static SharedPreferences sharedPreferences = null;

	public static void init(Context context) {
		sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
	}

	public static void destroy() {
		sharedPreferences = null;
	}

	public static SharedPreferences getSharedPreferences() {
		return sharedPreferences;
	}

	public static int getTimeout() {
		return sharedPreferences.getInt(TIMEOUT, 100);
	}
}
