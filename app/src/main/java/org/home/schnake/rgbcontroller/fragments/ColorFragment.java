package org.home.schnake.rgbcontroller.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import org.home.schnake.rgbcontroller.R;
import org.home.schnake.rgbcontroller.ScrollableSwipeRefreshLayout;
import org.home.schnake.rgbcontroller.colorpicker.ColorChangedListener;
import org.home.schnake.rgbcontroller.colorpicker.ColorPicker;
import org.home.schnake.rgbcontroller.databinding.FragmentColorBinding;
import org.home.schnake.rgbcontroller.daten.Colors;
import org.home.schnake.rgbcontroller.daten.NodeInfo;
import org.home.schnake.rgbcontroller.daten.Preferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ColorFragment extends Fragment {

	private static final String TAG = "ColorFragment";

	private static final String ARG_INDEX = "ARG_INDEX";
	private static final String ARG_COAP = "ARG_COAP";

	final private static int MAX_VALUE = 8191;

	protected Colors colors = null;

	protected NodeInfo nodeInfo = null;
	protected int index;

	@BindView(R.id.colorpicker_rgb)
	ColorPicker colorPickerRgb;
	@BindView(R.id.seekbar_white)
	SeekBar seekbarWhite;
	@BindView(R.id.seekbar_brightness)
	SeekBar seekbarBrightness;
	@BindView(R.id.led_color_frag_swipe)
	ScrollableSwipeRefreshLayout scrollableSwipeRefreshLayout;

	private Unbinder unbinder = null;
	protected FragmentColorBinding fragmentColorBinding = null;

	public ColorFragment() { }

	public static ColorFragment newInstance(int index, NodeInfo nodeInfo) {
		ColorFragment fragment = new ColorFragment();

		Bundle bundle = new Bundle();
		bundle.putInt(ARG_INDEX, index);
		bundle.putParcelable(ARG_COAP, nodeInfo);
		fragment.setArguments(bundle);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		fragmentColorBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_color, container, false);
		View view = fragmentColorBinding.getRoot();

		unbinder = ButterKnife.bind(this, view);

		Bundle bundle = getArguments();
		if(bundle!=null) {
			index = bundle.getInt(ARG_INDEX);
			nodeInfo = bundle.getParcelable(ARG_COAP);
			Log.d(TAG, "nodeInfo1:"+ nodeInfo);
		}

		colors = new Colors(nodeInfo);
		colors.setIndex(index);
		loadColorsFormDevice();

		fragmentColorBinding.setHandlers(this);
		fragmentColorBinding.setColors(colors);

		initListener();
		initRefresh();

		return view;
	}

	protected void loadColorsFormDevice() {
		new AsyncTask<Void, Void, Boolean>() {
			@Override
			protected Boolean doInBackground(Void... params) {
				return colors.loadData();
			}

			@Override
			protected void onPostExecute(Boolean aBoolean) {
				super.onPostExecute(aBoolean);

				setRGB(colors.getR(), colors.getG(), colors.getB());
				seekbarWhite.setProgress(colors.getWhite());
				seekbarBrightness.setProgress(colors.getBrightness());
			}
		}.execute();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		if(unbinder!=null) {
			unbinder.unbind();
			unbinder = null;
		}

		nodeInfo = null;
		colors = null;
	}

	public void setRGB(int r, int g, int b) {
		Log.v(TAG, "setRGB: ("+ r +"/"+ g +"/"+ b +")");
		colorPickerRgb.setColor((0xff&r)<<16 | (0xff&g)<<8 | (0xff&b));
	}

	protected void initListener() {
		Log.d(TAG, "initListener");

		colorPickerRgb.setColorChangedListener(new ColorChangedListener() {
			@Override
			public void onColorChanged(int color, int oldColor) {
				new AsyncTask<Integer, Void, Boolean>() {
					@Override
					protected Boolean doInBackground(Integer... params) {
						colors.setRgb(params[0]);
						return colors.storeData();
					}

					@Override
					protected void onPostExecute(Boolean aBoolean) {
						super.onPostExecute(aBoolean);
					}
				}.execute(color);
			}
		});

		seekbarWhite.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				new AsyncTask<Integer, Void, Boolean>() {
					@Override
					protected Boolean doInBackground(Integer... params) {
						colors.setWhite(params[0]);
						return colors.storeData();
					}

					@Override
					protected void onPostExecute(Boolean aBoolean) {
						super.onPostExecute(aBoolean);
					}
				}.execute(progress);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});

		seekbarBrightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				new AsyncTask<Integer, Void, Boolean>() {
					@Override
					protected Boolean doInBackground(Integer... params) {
						colors.setBrightness(params[0]);
						return colors.storeData();
					}

					@Override
					protected void onPostExecute(Boolean aBoolean) {
						super.onPostExecute(aBoolean);
					}
				}.execute(progress);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});
	}

	protected void initRefresh() {
		scrollableSwipeRefreshLayout.setEnabled(false);
		scrollableSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				loadColorsFormDevice();
			}
		});
	}
}
