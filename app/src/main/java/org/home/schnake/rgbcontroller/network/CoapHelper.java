package org.home.schnake.rgbcontroller.network;

import android.os.Parcelable;
import android.util.Log;

import com.google.gson.Gson;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.core.network.config.NetworkConfigDefaults;
import org.home.schnake.rgbcontroller.daten.NodeInfo;

public class CoapHelper {
	private static final String TAG = "CoapHelper";

	final protected Object syncNetworkAccess = new Object();

	protected NodeInfo nodeInfo = null;

	public static class RGBW {
		protected Integer i = null;
		protected Integer r = null;
		protected Integer g = null;
		protected Integer b = null;
		protected Integer w = null;
		protected Integer brightness = null;

		public Integer getI() {
			return i;
		}
		public void setI(Integer i) {
			this.i = i;
		}

		public Integer getR() {
			return r;
		}
		public void setR(Integer r) {
			this.r = r;
		}

		public Integer getG() {
			return g;
		}
		public void setG(Integer g) {
			this.g = g;
		}

		public Integer getB() {
			return b;
		}
		public void setB(Integer b) {
			this.b = b;
		}

		public Integer getW() {
			return w;
		}
		public void setW(Integer w) {
			this.w = w;
		}

		public void setRGB(int r, int g, int b) { this.r = r; this.g = g; this.b = b; }
		public void setRGBW(int r, int g, int b, int w) { this.r = r; this.g = g; this.b = b; this.w = w; }

		public Integer getBrightness() {
			return brightness;
		}
		public void setBrightness(Integer brightness) {
			this.brightness = brightness;
		}

		public String toString() {
			return "(i:" + i + ", r:" + r + ", g:" + g + ", b:" + b + ", w:" + w + ", brightness: " + brightness + ")";
		}
	}

	public static class Settings {
		protected Float gamma = null;
		protected String ssid = null;
		protected String password = null;
		protected String desc = null;

		public Float getGamma() {
			return gamma;
		}
		public void setGamma(Float gamma) {
			this.gamma = gamma;
		}

		public String getSsid() {
			return ssid;
		}
		public void setSsid(String ssid) {
			this.ssid = ssid;
		}

		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}

		public String getDesc() { return desc; }
		public void setDesc(String desc) { this.desc = desc; }

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("Settings: (gamma:");
			sb.append(gamma);
			sb.append(", ssid:");
			sb.append(ssid);
			sb.append(", password:");
			sb.append(password);
			sb.append(", desc:");
			sb.append(desc);
			sb.append(")");
			return sb.toString();
		}
	}

	public static RGBW createRGBW() {
		return new RGBW();
	}
	public static RGBW createRGBW(int r, int g, int b) {
		RGBW rgbw = new RGBW();
		rgbw.setRGB(r, g, b);
		return rgbw;
	}
	public static RGBW createRGBW(int r, int g, int b, int w) {
		RGBW rgbw = new RGBW();
		rgbw.setRGBW(r, g, b, w);
		return rgbw;
	}

	public static Settings createSettings() {
		return new Settings();
	}

	public CoapHelper(NodeInfo nodeInfo) {
		super();

		NetworkConfig nc = NetworkConfig.createStandardWithoutFile();
		NetworkConfigDefaults.setDefaults(nc);

		this.nodeInfo = nodeInfo;
	}

	public RGBW getRGBW(int index) {
		String json = "";
		CoapClient coapClient = new CoapClient("coap", nodeInfo.getHostAddress(), nodeInfo.getPort(), "/rgbw");
		synchronized(syncNetworkAccess) {
			CoapResponse coapResponse = coapClient.get();
			if(coapResponse!=null) {
				json = coapResponse.getResponseText();
			}
		}
		Log.d(TAG, "Response: " + json);

		Gson g = new Gson();
		RGBW[] rgbw = g.fromJson(json, RGBW[].class);
		if(rgbw != null) {
			if(rgbw.length > index) {
				Log.d(TAG, rgbw[index].toString());
			}
			return rgbw[index];
		}
		return null;
	}

	public boolean putRGBW(int index, RGBW rgbw) {
		boolean okay = false;
		rgbw.i = index;

		Gson gson = new Gson();
		String json = gson.toJson(rgbw);
		Log.d(TAG, "putRGBW: "+ json);

		CoapClient coapClient = new CoapClient("coap", nodeInfo.getHostAddress(), nodeInfo.getPort(), "/rgbw");
		synchronized(syncNetworkAccess) {
			CoapResponse coapResponse = coapClient.put(json.getBytes(), MediaTypeRegistry.APPLICATION_JSON);
			if(coapResponse != null && coapResponse.isSuccess()) {
				okay = true;
			}
		}
		return okay;
	}

	public Settings getSettings() {
		String json = "";
		synchronized(syncNetworkAccess) {
			CoapClient coapClient = new CoapClient("coap", nodeInfo.getHostAddress(), nodeInfo.getPort(), "/settings");
			CoapResponse coapResponse = coapClient.get();
			if(coapResponse!=null && coapResponse.isSuccess()) {
				json = coapResponse.getResponseText();
			}
		}

		Gson gson = new Gson();
		return gson.fromJson(json, Settings.class);
	}

	public boolean putSettings(Settings settings) {
		boolean okay = false;
		Gson gson = new Gson();
		String json = gson.toJson(settings);
		Log.d(TAG, "putSettings: "+ json);

		CoapClient coapClient = new CoapClient("coap", nodeInfo.getHostAddress(), nodeInfo.getPort(), "/settings");
		synchronized(syncNetworkAccess) {
			CoapResponse coapResponse = coapClient.put(json.getBytes(), MediaTypeRegistry.APPLICATION_JSON);
			coapClient.shutdown();
			if(coapResponse != null && coapResponse.isSuccess()) {
				okay = true;
			}
		}
		return okay;
	}
}
