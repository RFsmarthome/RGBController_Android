package org.home.schnake.rgbcontroller;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import org.home.schnake.rgbcontroller.fragments.ColorFragment;
import org.home.schnake.rgbcontroller.fragments.LedSettingsFragment;
import org.home.schnake.rgbcontroller.network.CoapHelper;
import org.home.schnake.rgbcontroller.daten.NodeInfo;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DeviceActivity extends AppCompatActivity {
	public static final String SERVICE_INFO_TAG = "serviceInfo";
	final private static String TAG = "DeviceActivity";

	final private static int INDEX = 0;
	final private static int MAX_VALUE = 8191;

	protected CoapHelper m_coapHelper;
	protected NodeInfo nodeInfo;

	@BindView(R.id.toolbar)
	Toolbar toolbar;
	@BindView(R.id.led_drawer_layout)
	DrawerLayout drawerLayout;
	@BindView(R.id.led_navigation_view)
	NavigationView navigationView;

	ActionBarDrawerToggle actionBarDrawerToggle = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_device);

		ButterKnife.bind(this);

		setSupportActionBar(toolbar);

		NodeInfo nodeInfo = getIntent().getParcelableExtra(SERVICE_INFO_TAG);
		this.nodeInfo = nodeInfo;
		m_coapHelper = new CoapHelper(nodeInfo);

		actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
		drawerLayout.addDrawerListener(actionBarDrawerToggle);

		getSupportFragmentManager().beginTransaction().replace(R.id.led_frame_layout, ColorFragment.newInstance(INDEX, nodeInfo)).commit();

		navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
			@Override
			public boolean onNavigationItemSelected(@NonNull MenuItem item) {
				selectDrawerMenu(item);
				return true;
			}
		});
	}

	protected void selectDrawerMenu(@NonNull MenuItem item) {
		Fragment fragment = DrawerManager.selectFragment(INDEX, nodeInfo, item);
		if(fragment != null) {
			activateFragment(item, fragment);
		}
		drawerLayout.closeDrawers();
	}

	private void activateFragment(@NonNull MenuItem item, Fragment fragment) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.led_frame_layout, fragment).commit();

		item.setChecked(true);
		setTitle(item.getTitle());
	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(actionBarDrawerToggle.onOptionsItemSelected(item)) {
			return  true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPostCreate(Bundle saveInstanceState) {
		super.onPostCreate(saveInstanceState);
		actionBarDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		actionBarDrawerToggle.onConfigurationChanged(newConfig);
	}

}
